---
hide:
- toc
---
## Integrating tabaqat in QGIS with WMTS and XYZ

Estimated reading time : 3 minutes

## Getting started

The following tutorial has been done on the latest version of QGIS. The process is however the same for older versions. Thanks to these instructions, you could use a WMS source to use your different map layers in QGIS.

## Requirements

Make sure that Browser view and Layers view are on. Click on the View tab on top of the QGIS window, then select Browser and Layers in the Panels section.

![alt text](../images/pages/qgis/requirements-01.png "Requirements for QGIS")

## XYZ Tutorial

First you need your XYZ endpoint. Get it from [tabaqat Platform](https://platform.tabaqat.net/).

That's what it looks like :

```
https://basemaps.tabaqat.net/your-style-id/{z}/{x}/{y}.png?access_token=your-tabaqat-access-token
```

You just need to replace your-tabaqat-access-token by your real access token that you can find in your account and your-style-id by your style ID. You can find your style id in Leaflet section in Style Manager:

> Tip: You can also use our default styles.

### Step 1

To import your XYZ layer in QGIS, go in the **Browser Panel**, find **XYZ Tiles**, right click on it and click on **New Connection...**

![alt text](../images/pages/qgis/xyz-new-connection.png "XYZ Connection")

### Step 2

A new window opens with settings to configure:

- **Name**: Name of the layer you will create. We will use tabaqat Sunny for our example, but you are free to choose what ever you want.
- **URL**: Link for XYZ tile source.
- **Max. Zoom Level**: You can set the max zoom level to 20.

Your new connection will be added in **XYZ** section on **Browser Panel**.

![alt text](../images/pages/qgis/xyz-url-connection.png "Add XYZ layer")

### step 3

Open your **XYZ** dropdown from you **Browser Panel** and find your new connection. Double click on your layer and voilà.

![alt text](../images/pages/qgis/view-basemap.png "Display on QGIS")

## WMTS Tutorial

First you need your WMTS endpoint. Get it from [tabaqat Platform](https://platform.tabaqat.net/). The WMTS endpoint is very simple :

```
https://basemaps.tabaqat.net/your-style-id/wmts?access_token=your-tabaqat-access-token

```

You just need to replace your-tabaqat-access-token by your real access token that you can find in your account and your-style-id by your style ID. You can find your style id in Leaflet section in Style Manager:

> Tip: You can also use our default styles.

### Step 1

To import your WMTS in QGIS, go in the **Browser Panel**, find **WMS**, right click on it and click on **New Connection...**

![alt text](../images/pages/qgis/wms-new-connection.png "Add WMS/WMTS Layer")

### Step 2

A new window opens with settings to configure:

- **Name**: Name of the layer you will create. We will use tabaqat for our example, but you are free to choose what ever you want.
- **URL**: Path to the WMTS file or HTTP link to import. We will use a HTTP link from tabaqat here.
  Other settings are left empty in our case. Your new connection will be added in **WMS** section on **Browser Panel**.

![alt text](../images/pages/qgis/wmts-url.png "Create a New WMS/WMTS Connection")

### Step 3

Open your **WMS** dropdown from you **Browser Panel** and find your new connection. Double click on your layer and voilà.

![alt text](../images/pages/qgis/view-wmts-basemap.png "Display on QGIS")
