---
hide:
- toc
---
# Get Map Service API Reference

Estimated reading time : 3 minutes

## Map Service API

The Map Service API allows you to retrieve WMS and WFS generated by the platform. You can use some libraries like Leaflet or MapLibre GL for JavaScript, Android or iOS to add a map to your website.

## Endpoint to use

If you need WFS URL, you should use this endpoint. We are supporting both WMS and WFS.

```
https://data.tabaqat.net/geoserver/wfs?access_token=your-tabaqat-access-token&AcceptLanguages=en&request=GetCapabilities
```

- **map service type**: you can set the map service to WFS or WMS.
- **authentication token**: create your access token on the platform.
- **language**: you can choose Arabic language or English language by setting this value to **en** for English or **ar** for Arabic.

## Note

Importing WFS layers is a **Premium** feature.

### Step 1

To import your WFS in QGIS, go in the **Browser Panel**, find **WFS/OGC API-Features**, right click on it and click on **New Connection...**

![alt text](../images/pages/qgis/wfs-01.png"Add WFS Layer")

### Step 2

A new window opens with settings to configure:

- **Name**: Name of the layer you will create. We will use tabaqat for our example, but you are free to choose what ever you want.
- **URL**: Path to the WFS file or HTTP link to import. We will use a HTTP link from tabaqat here.
  Other settings are left empty in our case. Your new connection will be added in **WFS/OGC API-Features** section on **Browser Panel**.

![alt text](../images/pages/qgis/wfs-02.png "Create a New WFS Connection")

### Step 3

Open your **WFS/OGC API-Features** dropdown from you **Browser Panel** and find your new connection. You can open the **tabaqat** dropdown further to find the vector data categories
![alt text](../images/pages/qgis/wfs-03.png "Display on QGIS")

### Step 4

Open **tabaqat** dropdown from you **Browser Panel** with data categories. Double click on your layer and voilà.

![alt text](../images/pages/qgis/wfs-04.png "Display on QGIS")

### Premium Step

You can do any selection, analysis, custom styling.

![alt text](../images/pages/qgis/wfs-05.png "Style on QGIS")