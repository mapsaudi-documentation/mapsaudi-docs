---
hide:
- toc
---
# Get Map Service API Reference

Estimated reading time : 3 minutes

## Map Service API
The Map Service API allows you to retrieve WMS and WFS generated by the **platform**. You can use some libraries like Leaflet or MapLibre GL for JavaScript, Android or iOS to add a map to your website.

## Endpoint to use
If you need WMS URL, you should use this endpoint. We are supporting both WMS and WFS.

```
https://data.tabaqat.net/geoserver/wms?access_token=your-tabaqat-access-token&AcceptLanguages=en&request=GetCapabilities

```

- **map service type**: you can set the map service to WMS or WFS.
- **authentication token**: create your access token on the platform.
- **language**: you can choose Arabic language or English language by setting this value to **en** for English or **ar** for Arabic.

### Step 1

To import your WMTS in QGIS, go in the **Browser Panel**, find **WMS**, right click on it and click on **New Connection...**

![alt text](../images/pages/qgis/wms-new-connection.png "Add WMS/WMTS Layer")

### Step 2

A new window opens with settings to configure:

- **Name**: Name of the layer you will create. We will use tabaqat for our example, but you are free to choose what ever you want.
- **URL**: Path to the WMTS file or HTTP link to import. We will use a HTTP link from tabaqat here.
  Other settings are left empty in our case. Your new connection will be added in **WMS** section on **Browser Panel**.

![alt text](../images/pages/qgis/qgis-png2.png "Create a New WMS/WMTS Connection")

### Step 3

Open your **WMS** dropdown from you **Browser Panel** and find your new connection. You can open the **tabaqat** dropdown further to find the data categories
![alt text](../images/pages/qgis/connection-success-tabaqat.png "Layers List Display on QGIS")

### Step 4

Open **tabaqat** dropdown from you **Browser Panel** with data categories. Double click on your layer and voilà.

![alt text](../images/pages/qgis/wms-layer.png "Display on QGIS")