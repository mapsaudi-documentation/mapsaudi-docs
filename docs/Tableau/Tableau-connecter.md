#<span style="color: #004D5C;">Develop Tableau Web Data Connector</span>
Estimated Reading: 30 minutes

###<span style="color: #004D5C;">Introduction</span>

Tableau web data connector **WDC** enables you to connect to [***tabaqat's***](https://tabaqat.net/) URLs. These URLs are used  to retrieve our data and use it in building visuals.


To connect tabaqat's URLs inside Tableau you have to pass through two stages:
1. Build WDC
2. Load data into Tableau

Now let's build WDC first using Tableau WDC SDK.

#####<span style="color: #004D5C;">WDC Prerequisites</span>

Before starting the development of WDC, you should have the following installed on your machine:

- [Git](https://git-scm.com/downloads)
- [node and npm](https://nodejs.org/en/download/)

#####<span style="color: #004D5C;">Get WDC</span>
Open the start menu and type terminal, then click on the command prompt as the following:

![Openning command prompt](../images/pages/tableau/open_command_prompt.png "Open start menu and type: terminal")

Change the folder path to the folder you want to put the web data connector folder in it. In this tutorial, I’ll use drive C: as my path.

```console
cd C:\
```
Run the following command to clone the webdataconnector folder in your path:

```console
git clone https://github.com/tableau/webdataconnector.git
```
Next change the directory to webdataconnector folder by running the following:
```console
cd webdataconnector
```
![Clonning WDC](../images/pages/tableau/cloning_WDC.png "Change directory and then clone WDC SDK")


>Note: You can select any folder you want, but don't forget to change the directory to it by using the **cd command!**
#####<span style="color: #004D5C;">Run the simulator</span>
The next step is to run WDC simulator in your web browser, but first let's install dependencies with npm:

- We still in the command prompt, type the following:
```console
npm install –production
```
- Run the simulator
```console
npm start
```
- Open your web browser and navigate to the following URL:
```console
http://localhost:8888/Simulator/index.html
```

![Open WDC simulator](../images/pages/tableau/Open_simulator_chrome.png "Open the simulator in chrome")

#####<span style="color: #004D5C;">Build WDC</span>
We are now ready to start coding and building our custom WDC to connect to tabaqat.

Go to the webdataconnector folder and open the Examples folder, you’ll find two folders: **HTML and js**, copy the **earthquakeUSGS** file from both folders and paste them into the webdataconnector folder. The next step is to rename both files to a convient name(Tabaqat would be perfect for our demo).

![Copying two example files](../images/pages/tableau/copy_html_js.png "Copy earthquakeUSGS files from html and js folders inside examples folder")
![Rename the files](../images/pages/tableau/rename_html_js.png "Rename the HTML and JS files into convient name (Ex: Tabaqat)")

>Remember: I've selected **the C drive** to download WDC SDK inside. If you had selected another location, you should navigate to.


#####<span style="color: #004D5C;">Modify HTML page</span>
Now we're ready to code. Tableau WDC is simply a web page programmed using HTML and javascript. we design the UI of the connector Using HTML.
Let us open **Tabaqat.html** file in any text editor and type the following code:

```html
<html>
<head>
    <title>Tabaqat Layers</title>
    <meta http-equiv="Cache-Control" content="no-store" />
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    
    <script src="https://connectors.tableau.com/libs/tableauwdc-2.3.latest.js" type="text/javascript"></script>
    <script src="Tabaqat.js" type="text/javascript"></script>
</head>

<body>
    <div class="container container-table">
        <div class="row vertical-center-row">
            
            
            
            
            <div class="text-center col-md-4 col-md-offset-4">
                <h2>Get Tabaqat Layers</h2>
<form>
	<div class="form-inline">
    <label for="start-date-one" class="text-center">Access Token</label>
	</div>
	<div class="form-inline">
		<input type="text" class="form-control" id="access-token" value="">
	</div>
</form>

                <button type="button" id="submitButton" class="btn btn-success" style="margin: 10px;">Get Tabaqat Data!</button>
            </div>
        </div>
    </div>
</body>

</html>
```
Now refresh the web browser you've already opened early to check the modifications using the simulator URL, change the connector URL to **Tabaqat.html**, and click the ***start interactive phase button***.

![Testing if the HTML page is designed properly so far](../images/pages/tableau/try_connector.png "Type: Tabqat.html in connector URL and then click start interactive phae")
![Testing if the HTML page is designed properly so far](../images/pages/tableau/try_connector_2.png "This is what should the connector look like.")


Now don’t worry, if you click the **Get Tabaqat Data!** button it won’t do anything, we must go to **Tabaqat.js** and add an event listener to tell the button what to do. 
#####<span style="color: #004D5C;">How to get access token from tabaqat?</span>
Before modifying the Javascript file, you need to get URL that gives you the data as a **GeoJSON** file.
1. First navigate to [tabaqat.net](https://tabaqat.net/) and create an account if you don't have one.
![Register at tabaqat.net](../images/pages/tableau/register_tabaqat.png "Click register and enter your user name, passwaord, and email")
2. After registering click login and sign with your email and password.
![Sign in at tabaqat.net](../Markdown/Images/register_tabaqat_2.png "Enter your email and password")
 
3. Change language to English
![Change Language](../images/pages/tableau/change_language.png "Click on الانجليزية to change language into english")

4. Click on **Key** then **Generate Key**. As a free user you have up to five keys to generate.
![Generate Key](../images/pages/tableau/toke_copy.png "tabaqat Token")

5. Head to **Map Services** and select the layer you want to load inside Tableau. You can copy *WMS* or *WFS*
![Map Services](../images/pages/tableau/get_link.png "")

Now let's get back to building the WDC!
#####<span style="color: #004D5C;">Modify Javascript File</span>
Before modifying the js code, you need to know that in WDC SDK there are two madatory functions:
* **GetSchema**
* **GetData**

From their name, the first one defines the table schema, i.e., how many tables, name of each table, how many columns in each table, column names, and data type of each column.
The other one gets the data from the API link (in our case [tabaqat](https://tabaqat.net/) URLs) and then fills the tables in the schema with the retrieved data.

Our API returns [**GeoJSON**](https://geojson.org/) format. You should be able to understand this format in order to decide which data to get and which to leave. 

Now for the sake of this demo, we will return two tables inside tableau using our tabaqat API links:

```
https://data.tabaqat.net/geoserver/education-and-training/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=education-and-training%3Aeducation-and-training_49OL1153845&outputFormat=application%2Fjson&access_token=

https://data.tabaqat.net/geoserver/education-and-training/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=education-and-training%3Aeducation-and-training_1YjB7179859&outputFormat=application%2Fjson&access_token=
```
The two links contain education and training indicators in **Riyadh** region. You can get these links using [tabaqat](https://tabaqat.net/). The links are missing access token at the end of each one. If you are a registered user at tabaqat you can get your access token and paste it at the end of the link after **=**.

#####<span style="color: #004D5C;">Tables Schema</span>
We expect to return two tables from the above two links. Open each link in [JSON formatter](https://jsonformatter.curiousconcept.com/#) and look at **properties** inside **features** object. That's how you decide the columns in each table.

>open **Tabaqat.js** in text editor and delete what is inside the red box in the next photo:

![Modify getSchema function](../images/pages/tableau/getSchema.png "Delete lines of code between the red box")

Now replace it with the following code:
```javascript
var cols_a = [
       
       {
        id: "objectid",
        dataType: tableau.dataTypeEnum.int
    }, {
        id: "governorate_id",
        dataType: tableau.dataTypeEnum.int
    }, {
        id: "governoratename_ar",
        dataType: tableau.dataTypeEnum.string
    }, {
        id: "governoratename_en",
        dataType: tableau.dataTypeEnum.string
    }, {
        id: "region_id",
        dataType: tableau.dataTypeEnum.int
    }, {
        id: "amana_id",
        dataType: tableau.dataTypeEnum.int
    }, {
        id: "governorate_unified_id",
        dataType: tableau.dataTypeEnum.int
    }, {
        id: "secondary_students_per_teacher_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "secondary_students_density_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "rented_secondary_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "owned_secondary_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "intermediate_students_per_teacher_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "intermediate_students_density_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "rented_intermediate_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "owned_intermediate_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "primary_students_per_teacher_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "primary_students_density_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "rented_primary_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "owned_primary_schools",
        dataType: tableau.dataTypeEnum.float
    }];    
    var cols_b = [ 
        
        {
        id: "city_id",
        dataType: tableau.dataTypeEnum.int
    }, {
        id: "name_ar",
        dataType: tableau.dataTypeEnum.string
    }, {
        id: "name_en",
        dataType: tableau.dataTypeEnum.string
    }, {
        id: "x",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "y",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "other_schools_students_density_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "other_teachers_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "other_students_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "other_classes_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "other_schools_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "other_rented_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "other_owned_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "special_education_teachers_number",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "special_education_students_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "special_education_classes_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "special_education_schools_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "special_education_rented_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "special_education_owned_schools",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "institutes_students_density_per_teacher_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "institutes_students_density_average",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "institutes_teachers_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "institutes_classes_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "institutes_no",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "rented_institutes",
        dataType: tableau.dataTypeEnum.float
    }, {
        id: "owned_institutes",
        dataType: tableau.dataTypeEnum.float
    }];
        
    var tbl_Schema_a = {
        id: "education_and_training_1YjB7179859",
        alias: "Tabaqat Riyadh Education Layer_2",
        columns: cols_a
    };
    var tbl_Schema_b = {
        id: "education_and_training_49OL1153845",
        alias: "Tabaqat Riyadh Education Layer_3",
        columns: cols_b
    };
     
    schemaCallback([tbl_Schema_a, tbl_Schema_b]);
```

Now let’s take a moment and explain briefly what this code does:
- The variables **cols_a** and **cols_b** are the columns that will be retrieved from the GeoJSON files
- The variables **tbl_Schema_a** and **tbl_Schema_a** represents the two table schemas that will be present inside Tableau. You can see that tbl_Schema_a has its id and columns, and tbl_Schema_b has its id and columns
- Function **schemaCallback** tells Tableau SDK what are tables to add inside tableau

>If you check column names and table names, you'll notice it is the same as the **GeoJSON** URLs. It is always a good practice to make the names identical.

#####<span style="color: #004D5C;">Getting data</span>
After defining the table schema, we need to get the data from the above two links. This can be done using the ***getData*** function. Delete the code inside the ***getData*** function as in the following photo:


![Deleter what's inside getJSON function](../images/pages/tableau/getJSON.png "Delete what's inside getJSON function")
Now, Replace it with the next code:
```javascript
if (table.tableInfo.id == "education_and_training_1YjB7179859") {
        var access_token = JSON.parse(tableau.connectionData);

            $.getJSON("https://data.tabaqat.net/geoserver/education-and-training/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=education-and-training%3Aeducation-and-training_1YjB7179859&outputFormat=application%2Fjson&access_token=" + access_token, function(resp) {
        var feat = resp.features,
            tbl_Data_a = [];

        // Iterate over the JSON object
        for (var i = 0, len = feat.length; i < len; i++) {
            tbl_Data_a.push({
                "objectid": feat[i].properties.objectid,
                "governorate_id": feat[i].properties.governorate_id,
                "governoratename_ar": feat[i].properties.governoratename_ar,
                "governoratename_en": feat[i].properties.governoratename_en,
                "region_id": feat[i].properties.region_id,
                "amana_id": feat[i].properties.amana_id,
                "governorate_unified_id": feat[i].properties.governorate_unified_id,
                "secondary_students_per_teacher_average": feat[i].properties.secondary_students_per_teacher_average,
                "secondary_students_density_average": feat[i].properties.secondary_students_density_average ,
                "rented_secondary_schools": feat[i].properties.rented_secondary_schools ,
                "owned_secondary_schools": feat[i].properties.owned_secondary_schools ,
                "intermediate_students_per_teacher_average": feat[i].properties.intermediate_students_per_teacher_average ,
                "intermediate_students_density_average": feat[i].properties.intermediate_students_density_average ,
                "rented_intermediate_schools": feat[i].properties.rented_intermediate_schools ,
                "owned_intermediate_schools": feat[i].properties.owned_intermediate_schools ,
                "primary_students_per_teacher_average": feat[i].properties.primary_students_per_teacher_average ,
                "primary_students_density_average": feat[i].properties.primary_students_density_average ,
                "rented_primary_schools": feat[i].properties.rented_primary_schools ,
                "owned_primary_schools": feat[i].properties.owned_primary_schools 
            });
        }
            table.appendRows(tbl_Data_a);
            doneCallback();
        });

        
    }
    else if (table.tableInfo.id == "education_and_training_49OL1153845") {
        
                 var access_token = JSON.parse(tableau.connectionData);

            $.getJSON("https://data.tabaqat.net/geoserver/education-and-training/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=education-and-training%3Aeducation-and-training_49OL1153845&outputFormat=application%2Fjson&access_token=" + access_token, function(resp) {
        var feat = resp.features,
            tbl_Data_b = [];

        // Iterate over the JSON object
        for (var i = 0, len = feat.length; i < len; i++) {
            tbl_Data_b.push({
                "city_id": feat[i].properties.city_id,
                "name_ar": feat[i].properties.name_ar,
                "name_en": feat[i].properties.name_en,
                "x": feat[i].properties.x,
                "y": feat[i].properties.y,
                "region_id": feat[i].properties.region_id,
                "other_schools_students_density_average": feat[i].properties['other_schools_students_density average'],
                "other_teachers_schools": feat[i].properties.other_teachers_schools,
                "other_students_no": feat[i].properties['other_students_no.'] ,
                "other_classes_no": feat[i].properties['other_classes_no.'] ,
                "other_schools_no": feat[i].properties['other_schools_no.'] ,
                "other_rented_schools": feat[i].properties.other_rented_schools ,
                "other_owned_schools": feat[i].properties.other_owned_schools ,
                "special_education_teachers_number": feat[i].properties.special_education_teachers_number ,
                "special_education_students_no": feat[i].properties['special_education_students_no.'] ,
                "special_education_classes_no": feat[i].properties['special_education_classes_no.'] ,
                "special_education_schools_no": feat[i].properties['special_education_schools_no.'] ,
                "special_education_rented_schools": feat[i].properties.special_education_rented_schools ,
                "special_education_owned_schools": feat[i].properties.special_education_owned_schools,
                "institutes_students_density_per_teacher_average": feat[i].properties.institutes_students_density_per_teacher_average ,
                "institutes_students_density_average": feat[i].properties.institutes_students_density_average ,
                "institutes_teachers_no": feat[i].properties['institutes_teachers_no.'] ,
                "institutes_students_no": feat[i].properties['institutes_students_no.'], 
                "institutes_classes_no": feat[i].properties['institutes_classes_no.'],
                "institutes_no": feat[i].properties['institutes_no.'],
                "rented_institutes": feat[i].properties.rented_institutes,
                "owned_institutes": feat[i].properties.owned_institutes
                
            });
        }
            table.appendRows(tbl_Data_b);
            doneCallback();
        });  
    }

```
A simple explanation of the above code is that we’ve made an if the condition that decides if the table we are calling is from the first link, we use the ***$.getJSON*** function with the first table, and the same for the other table. Then inside each condition, there is a for loop that loops through the GeoJSON file and appends the data in tabular form.


#####<span style="color: #004D5C;">Final Step</span>
We've made the hard word so far. The final step is an easy one to finalize building the WDC. Look at the next image and replace the code from 1 with 2 in the next image:
![Modify ready function](../images/pages/tableau/ready_fn.png "replace 1 with 2")
You can copy the code as follow:
```javascript
$("#submitButton").click(function() {
            var access_token =  $('#access-token').val().trim(); // The access token variable that the user will enter inside WDC
            tableau.connectionData = JSON.stringify(access_token);
            tableau.connectionName = "Ryiadh Tabaqat"; // This will be the data source name in Tableau
            tableau.submit(); // This sends the connector object to Tableau
        });
```
#####<span style="color: #004D5C;">Test The Connector in The Web Browser</span>
Now open any browser and type: http://localhost:8888/Simulator/index.html, then nvaigate to this webpage. In the connector URL write: **../Tabqat.html**, then click start interactive phase button. It will open our connector web page. Enter **your access token** you've already got from tabaqat.net and click Get Tabaqat Data! button. The instructions are shown in the next two photos.

 ![Open Simulator](../images/pages/tableau/Open_simulator_chrome.png "Open any web browser and navigate to simulator link and open it!")

 ![Try out the connector](../images/pages/tableau/enter_access_token.png "Type your access token and click Get Tabaqat Data!")

Scroll down the page and click ***Fetch Table Data***, and the connector will return the first table. Scroll down again, and you’ll find another button click on it, and it will return the second table.

![Fetching Data](../images/pages/tableau/fetch_data.png "Click Fetch Table Data button to check the data!")

###<span style="color: #004D5C;">Using Connector in Tableau</span>
We've finished everything related to developing the WDC. The WDC can be published and hosted on a server to avoid running **npm** every time you need to load the data, but that's not our concern right now. Let's pull the data from [**tabaqat's**](https://tabaqat.net/) URLs and load it into Tableau


1. Open Tableau Desktop

![Openning Tableau Desktop](../images/pages/tableau/open_tableau_desktop.png "Click on tableau icon on the top left")
2. Below To a Server select more

![Select More](../images/pages/tableau/click_more_WDC.png "Click on more below connect to a server")
3. From more select Web Data Conector

![Click Web Data Connector](../images/pages/tableau/wdc.png "Choose Web Data Connector")

4. Tableau will open web browser, type the locally hosted connector link which is: 

```
http://localhost:8888/Tabaqat.html
```
>**Tabaqat.html** is the name of our HTML page we've created. If you did rename it other name type it insted of Tabaqat.

![Type the connector URL](../images/pages/tableau/connector_link.png "The connector URL should be: http://localhost:8888/Tabaqat.html")

5. Tableau will open the connector that we’ve made and it will ask you for **access token**, enter it and click **Get Tabaqat Data!**, tableau will retrieve the data from the links and hence we have our tables.

![Enter access token](../images/pages/tableau/enter_access_token2.png "You should enter the access token you've gotten from tabaqat")

![Two tables](../images/pages/tableau/tables_tableau.png "You can see the two tables tableau retrieved")
Now you are ready to build visuals using tableau.

6. Drop Tables into conncetion area and build relationships or joins

![Connect tables together using joins, append, or connection](../images/pages/tableau/drop_tables.png "You can select one or more table to drop in the connection area")

7. Build your visuals using the data

![Now it is time to build visuals using Tableau](../images/pages/tableau/build_visuals.png "You can build any visual using the given data")
