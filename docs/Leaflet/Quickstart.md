# Leaflet (raster map)

Estimated reading time : 2 minutes

## Prerequisite

For all of our examples, you will need:

- An API key and tabaqat account ([from the platform](https://platform.tabaqat.net/)),
- Basic knowledge in JavaScript and HTML,
- One style (default or custom style)

## Get the library

There are several ways to get this library:

- from the github repository ([github.com/Leaflet/Leaflet](https://github.com/Leaflet/Leaflet))
- from npmjs.com (package name: [leaflet](https://www.npmjs.com/package/leaflet))
- from a CDN (e.g unpkg.com)

```
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"></script>
```

## Attribution

Your map **must** display the following links: [tabaqat]() and © [OpenStreetMap](https://www.openstreetmap.org/about/).

This is our attribution template:

```
<a href="https://basemaps.tabaqat.net/" title="Tiles Courtesy of tabaqat Maps" target="_blank" class="jawg-attrib" >&copy; <b>tabaqat</b>Maps</a> | <a href="https://www.openstreetmap.org/copyright" title="OpenStreetMap is open data licensed under ODbL" target="_blank" class="osm-attrib" >&copy; OSM contributors</a>
```

## Our examples

- [simple-map](./Simple-map.md): Simple map integration
- [add-marker](./Add-a-marker.md): Add a marker on your map
- [add-geometry](./Add-Geometry.md): Add a geometry from GeoJSON on your map
- [add-popup](./Display-a-popup.md): Add a popup on your map
- [change-language](./Change-language.md): Change your map's language
- [change-style](./Change-style.md): Change your map style (with our default styles)
- [custom-style](./Use-a-custom-style.md): Use a custom style from [tabaqat Platform](https://platform.tabaqat.net/)
- [maplibre-gl-leaflet](./Use-maplibre-gl-leaflet.md): Use vector tiles with maplibre-gl-leaflet