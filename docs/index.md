# tabaqat Documentation

In this documentation, you will have access to examples, tutorials and API references to help you start building with tabaqat Maps. Start with our API references and then look at our examples and tutorials.

# API doc & reference

<div class="main-wrapper">

    <div class="card">
        <a href=./BaseMaps>
            <img class="home-icon" src="./images/home/basemaps.svg" >
            <div class="text">
                <h4><b>Base Maps</b></h4>
                <p>Get a map on your website</p>
            </div>
        </a>
    </div>

    <div class="card">
        <a href=./mapservices/WMS>
            <img class="home-icon" src="./images/home/mapservices.png" >
            <div class="text">
                <h4><b>Map Services</b></h4>
                <p>tabaqat Services</p>
            </div>
        </a>
    </div>

</div>

# SDKs & Softwares

<div class="main-wrapper">
    <div class="card">
        <a href=./QGIS/QGIS>
            <img class="home-icon" src="./images/home/qgis.png" >
            <div class="text">
                <h4><b>QGIS</b></h4>
                <p>tabaqat in QGIS</p>
            </div>
        </a>
    </div>

    <div class="card">
        <a href=./ArcMap/WMS>
            <img class="home-icon" src="./images/home/arcmap.png" >
            <div class="text">
                <h4><b>ArcMap</b></h4>
                <p>tabaqat in ArcMap</p>
            </div>
        </a>
    </div>

    <div class="card">
        <a href=./ArcGIS%20Pro/WMS/>
            <img class="home-icon" src="./images/home/arcgispro.png" >
            <div class="text">
                <h4><b>ArcGIS Pro</b></h4>
                <p>tabaqat in ArcGIS Pro</p>
            </div>
        </a>
    </div>

    <div class="card">
        <a href=./Power%20BI/>
            <img class="home-icon" src="./images/icons/powerbi.png" >
            <div class="text">
                <h4><b>Power BI</b></h4>
                <p>tabaqat in Power BI</p>
            </div>
        </a>
    </div>
    <!-- <div class="card">
        <a href=./MapLibreJs/Quickstart>
            <img class="home-icon" src="./images/home/MapLibreJS.png" >
            <div class="text">
                <h4><b>MapLibre GL JS (vector map)</b></h4>
                <p>tabaqat with MapLibre GL JS (recommended)</p>
            </div>
        </a>
    </div>

    <div class="card">
        <a href=./Leaflet/Quickstart>
            <img class="home-icon" src="./images/home/leaflet.png" >
            <div class="text">
                <h4><b>Leaflet (raster map)</b></h4>
                <p>tabaqat with Leaflet</p>
            </div>
        </a>
    </div>

    <div class="card">
        <a href=./Maplibre-android/Quickstart>
            <img class="home-icon" src="./images/home/android.png" >
            <div class="text">
                <h4><b>MapLibre GL Android</b></h4>
                <p>tabaqat with MapLibre Android SDK</p>
            </div>
        </a>
    </div>

    <div class="card">
        <a href=./Mapbox-ios/Quickstart>
            <img class="home-icon" src="./images/home/ios.png" >
            <div class="text">
                <h4><b>Mapbox GL iOS</b></h4>
                <p>tabaqat With Mapbox iOS SDK</p>
            </div>
        </a>
    </div>

    <div class="card">
        <a href=./ReactNative/Quickstart>
            <img class="home-icon" src="./images/home/react.png" >
            <div class="text">
                <h4><b>React Native</b></h4>
                <p>tabaqat with React Native</p>
            </div>
        </a>
    </div> -->

</div>

# About the doc

We strongly believe in practical approaches, and we tried to cover and combine relevant samples for you to use and make your integration much easier.

If you are looking for more thorough and detailed integrations, we have both real-life apps and links to the original SDKs and libraries which will give you what you want without any doubt.

Still struggling? No worries, our team will be happy to answer your technical questions via Twitter [@tabaqat_Inc](https://twitter.com/tabaqat_Inc) in either Arabic or English.

Feel free to get in touch if you need, we will be happy to help.