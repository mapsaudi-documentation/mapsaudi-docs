---
hide:
- toc
---
## Integrating tabaqat in ArcGIS Pro with WMTS and XYZ

Estimated reading time : 3 minutes

## Getting started

The following tutorial has been done on the latest version of ArcGIS Pro. The process is however the same for older versions. Thanks to these instructions, you could use a WMS source to use your different map layers in ArcGIS Pro.

## Requirements

Make sure that Catalog Pane and Contents are on. Click on the View tab on top of the ArcGIS Pro window, then click on Catalog Pane and Contents in the Panel section.

![alt text](../images/pages/arcgispro/requirements-001.PNG "Requirements for ArcGIS Pro")

## WMTS Tutorial

First you need your WMTS endpoint. Get it from [tabaqat Platform](https://platform.tabaqat.net/). The WMTS endpoint is very simple :

```
https://basemaps.tabaqat.net/your-style-id/wmts.xml?access_token=your-tabaqat-access-token

```

You just need to replace your-tabaqat-access-token by your real access token that you can find in your account and your-style-id by your style ID. You can find your style id in Leaflet section in Style Manager:

> Tip: You can also use our default styles.

### Step 1

To import your WMTS in ArcGIS Pro, go in the **Catalog Pane**, right click in there and select **New** then select from the sub menu **New server**, then select **New WMTS server**

![alt text](../images/pages/arcgispro/arcpro-02.PNG "Add WMS/WMTS Layer")

### Step 2

A new window opens with settings to configure:

- **Server URL**:  Path to the WMTS file or HTTP link to import. We will use a HTTP link from tabaqat here.
  Other settings are left empty in our case.
 Your new connection will be added in Projects tab in the Catalog in **Servers**.

![alt text](../images/pages/arcgispro/arcpro-basemap.PNG "Create a New WMS/WMTS Connection")

### Step 3

![alt text](../images/pages/arcgispro/arcpro-basemap02.PNG "Open your server connection")
Open your **tabaqat connection** dropdown from you **Servers** and find your new connection. Drag and drop your layer in the map windows and voilà.

![alt text](../images/pages/arcgispro/arcpro-basemap03.PNG "Display on ArcGIS pro")
