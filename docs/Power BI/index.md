---
hide:
- toc
---

# Power BI

Estimated reading time : 3 minutes

## Connect to Tabaqat Link
1-Open **Power BI** Desktop and Connect to Web.

![Open Power BI Desktop and Connect to Web](../images/pages/powerbi/Open_Power_BI_Desktop.png)


2-Migrate URL and access token from [***tabaqat.net***](https://tabaqat.net/)

![Migrate URL and access token](../images/pages/powerbi/Migrate_URL_and_access_token.PNG)

3-**Power BI** will forward you automatically to **Power Query editor**.

![Power Query editor](../images/pages/powerbi/Power_Query_editor.PNG)

4-Press on **Expand to New Rows**.

![Expand to New Rows](../images/pages/powerbi/Expand_to_New_Rows.PNG)

5-Expand Features again and Choose Properties.

![Expand Features again and Choose Properties](../images/pages/powerbi/Expand_Features_again.PNG)

6-Expand Properties and Select All Columns.

![Expand Properties and Select All Columns](../images/pages/powerbi/Expand_Properties_and_Select_All_Columns.PNG)

7-Rename the table to convient name and then click **Close and Apply**.

![Expand Properties and Select All Columns](../images/pages/powerbi/step7.png)

8-Build your visuals using the data

![visual](../images/pages/powerbi/visual.PNG)