---
hide:
- toc
---
# Maps

Estimated reading time : 1 minutes

## WMTS
If you want to share a preview of your map, use this endpoint.

```
https://basemaps.tabaqat.net/styles/your-style-id/wmts.xml?access_token=your-tabaqat-access-token
```

- **style ID**: your style ID or one of our default styles or your custom style from the platform.
- **authentication** token: create your access token on the Keys page.
- **preview type** (optional): choose your preview in raster or vector. Valid values are raster, vector.

## XYZ

The Maps API allows you to retrieve vector or raster tiles generated by the lab. You can use some libraries like Leaflet or MapLibre GL for JavaScript, Android or iOS to add a map to your website.

## Endpoint to use

If you need tile URL, you should use this endpoint. We are supporting both raster png and vector pbf tiles.

```
https://basemaps.tabaqat.net/styles/your-style-id.json/{z}/{x}/{y}.png?access_token=your-tabaqat-access-token
```

- **style ID**: choose your style ID from one of our styles:

    1. basic-arabic-ksa-boundary
    2. bright-arabic-ksa-boundary
    3. dark-matter-arabic-ksa-boundary
    4. basic-arabic-ksa-buildings
    5. bright-arabic-ksa-boundary-secondary
    6. outdoor
    7. bright
  
- **authentication token**: create your access token on the platform.

## Vector Tiles (Style GL)
If you want your style as json for MapLibre GL you will need this endpoint:

```
https://basemaps.tabaqat.net/styles/your-style-id.json?access_token=your-tabaqat-access-token
```

- **style ID**: your style ID or one of our default styles or your custom style from the platform.
- **authentication token**: create your access token on the platform.

## tabaqat default styles

We have some default styles which can be used by anyone with an access token. Here is our list :

<div class="main-wrapper">
    <div class="card_maps">
            <div class="text">
                <h4><b>Basic Arabic KSA Boundary</b></h4>
                <p>basic-arabic-ksa-boundary</p>
            </div>
            <img class="img_maps" src="../images/basemaps/BasicBoundary.png" >
    </div>

    <div class="card_maps">
            <div class="text">
                <h4><b>Bright Arabic KSA Boundary</b></h4>
                <p>bright-arabic-ksa-boundary</p>
            </div>
            <img class="img_maps" src="../images/basemaps/DarkBoundary.png" >
    </div>

    <div class="card_maps">
            <div class="text">
                <h4><b>Dark Matter Arabic KSA Boundary</b></h4>
                <p>dark-matter-arabic-ksa-boundary</p>
            </div>
            <img class="img_maps" src="../images/basemaps/BasicBuildings.png" >
    </div>

    <div class="card_maps">
            <div class="text">
                <h4><b>Basic Arabic KSA Buildings</b></h4>
                <p>basic-arabic-ksa-buildings</p>
            </div>
            <img class="img_maps" src="../images/basemaps/BrightSecondary.png" >
    </div>

    <div class="card_maps">
            <div class="text">
                <h4><b>Outdoor</b></h4>
                <p>Style ID : outdoor</p>
            </div>
            <img class="img_maps" src="../images/basemaps/Bright.png" >
    </div>

    <div class="card_maps">
            <div class="text">
                <h4><b>Bright</b></h4>
                <p>Style ID : bright</p>
            </div>
            <img class="img_maps" src="../images/basemaps/Outdoor.png" >
    </div>


    <div class="card_maps">
            <div class="text">
                <h4><b>Bright Arabic KSA Boundary Secondary</b></h4>
                <p>bright-arabic-ksa-boundary-secondary</p>
            </div>
            <img class="img_maps" src="../images/basemaps/BrightBoundary.png" >
    </div>

</div>
